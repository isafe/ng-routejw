import { Component, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/map';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profile: any = [];
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    Observable.combineLatest([
      this.route.queryParamMap,
      this.route.paramMap
    ])
    //.map(combined => { // will return Observable<any>
    .switchMap(combined => {  // will return any
      let id = combined[0].get('id');
      let page = combined[1].get('page');

      return this.service.getAll();
    }).subscribe( profile => { this.profile = profile; console.log(profile); });

    this.route.queryParamMap.subscribe(query => {
        let query1 = +query.get('id');
    });

    this.route.paramMap.subscribe(value => {
      let param = +value.get('id'); //convert from string type to number
      let tp = typeof param;
      console.log('params from router: ', param);
      console.log('params type: ', tp);
    })  ;
  }

}
