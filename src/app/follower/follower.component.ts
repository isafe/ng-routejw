import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-follower',
  templateUrl: './follower.component.html',
  styleUrls: ['./follower.component.css']
})
export class FollowerComponent implements OnInit {

  constructor(private route: Router) {
   }

   submit() {
     this.route.navigate(
        ['/followers', 1, 'name'], //url, input variables
        {queryParams: {page: 1, total: 20} }, //query strings
     )
   }

  ngOnInit() {
  }


}
